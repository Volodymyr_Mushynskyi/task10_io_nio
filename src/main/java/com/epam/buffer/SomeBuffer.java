package com.epam.buffer;

import java.nio.ByteBuffer;

public class SomeBuffer {
  private byte[] bufferArr;
  private ByteBuffer byteBuffer;

  public SomeBuffer(int bufferSize) {
    this.bufferArr = new byte[bufferSize];
    this.byteBuffer = ByteBuffer.wrap(bufferArr);
  }

  public ByteBuffer getByteBuffer() {
    return this.byteBuffer;
  }

  public void put(byte[] value) {
    byteBuffer.put(value);
  }

  public void put(int index, byte value) {
    byteBuffer.put(index, getByteBuffer(index)).put(value);
  }

  public byte getByteBuffer(int index) {
    return byteBuffer.get(index);
  }

  public void flip() {
    byteBuffer.limit(byteBuffer.position()).position(0);
  }

  public boolean hasRemaining() {
    return byteBuffer.hasRemaining();
  }

  public byte get() {
    return byteBuffer.get();
  }

  public void clear() {
    byteBuffer.clear();
  }


}
