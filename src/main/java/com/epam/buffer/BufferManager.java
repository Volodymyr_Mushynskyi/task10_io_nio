package com.epam.buffer;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;

import static com.sun.xml.internal.messaging.saaj.packaging.mime.util.ASCIIUtility.getBytes;


public class BufferManager {

  public void showBuffer() {
    try {
      write();
      read();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void write() throws IOException {
    RandomAccessFile file = new RandomAccessFile("data.txt", "rw");
    FileChannel channel = file.getChannel();
    SomeBuffer someBuffer = new SomeBuffer(100);
    String string = "Hello";
    channel.write(someBuffer.getByteBuffer().put(getBytes(string)));
    file.close();
  }

  private void read() throws IOException {
    RandomAccessFile file = new RandomAccessFile("data.txt", "rw");
    FileChannel channel = file.getChannel();
    SomeBuffer someBuffer = new SomeBuffer(100);
    int bytesRead = channel.read(someBuffer.getByteBuffer());
    while (bytesRead != -1) {
      someBuffer.flip();
      while (someBuffer.hasRemaining()) {
        System.out.print((char) someBuffer.get());
      }
      someBuffer.clear();
    }
    System.out.println(bytesRead);
    file.close();
  }
}

