package com.epam.clientserver;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

import static com.epam.constant.Const.BUFFER_SIZE;
import static com.epam.constant.Const.PORT;

public class Server {

  private static Selector selector;

  public static void main(String[] args) {
    try {
      InetAddress hostIP = InetAddress.getLocalHost();
      selector = Selector.open();
      ServerSocketChannel mySocket = ServerSocketChannel.open();
      ServerSocket serverSocket = mySocket.socket();
      InetSocketAddress address = new InetSocketAddress(hostIP, PORT);
      serverSocket.bind(address);
      mySocket.configureBlocking(false);
      int ops = mySocket.validOps();
      mySocket.register(selector, ops, null);
      while (true) {
        selector.select();
        Set<SelectionKey> selectedKeys = selector.selectedKeys();
        Iterator<SelectionKey> i = selectedKeys.iterator();
        while (i.hasNext()) {
          SelectionKey key = i.next();
          if (key.isAcceptable()) {
            processAcceptEvent(mySocket, key);
          } else if (key.isReadable()) {
            processReadEvent(key);
          }
          i.remove();
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void processAcceptEvent(ServerSocketChannel mySocket, SelectionKey key) throws IOException {
    SocketChannel myClient = mySocket.accept();
    myClient.configureBlocking(false);
    myClient.register(selector, SelectionKey.OP_READ);
  }

  private static void processReadEvent(SelectionKey key) throws IOException {
    SocketChannel myClient = (SocketChannel) key.channel();
    ByteBuffer myBuffer = ByteBuffer.allocate(BUFFER_SIZE);
    myClient.read(myBuffer);
    String data = new String(myBuffer.array()).trim();
    if (data.length() > 0) {
      if (data.equalsIgnoreCase("*exit*")) {
        myClient.close();
      }
    }
  }
}
