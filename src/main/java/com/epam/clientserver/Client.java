package com.epam.clientserver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

import static com.epam.constant.Const.BUFFER_SIZE;
import static com.epam.constant.Const.PORT;

public class Client {

  private static Logger logger = LogManager.getLogger(Client.class);
  private static Selector selector;
  private static String[] messages = {"Hello from client"};

  public static void main(String[] args) throws IOException {
    selector = Selector.open();
    InetAddress hostIP = InetAddress.getLocalHost();
    InetSocketAddress myAddress = new InetSocketAddress(hostIP, PORT);
    SocketChannel myClient = SocketChannel.open(myAddress);
    for (String message : messages) {
      ByteBuffer myBuffer = ByteBuffer.allocate(BUFFER_SIZE);
      myBuffer.put(message.getBytes());
      myBuffer.flip();
      logger.info("Send message :" + message);
    }
  }
}

