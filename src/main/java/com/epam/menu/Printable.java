package com.epam.menu;

public interface Printable {
  void print();
}
