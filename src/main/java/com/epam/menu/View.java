package com.epam.menu;

import com.epam.buffer.BufferManager;
import com.epam.bufferreadwrite.Performance;
import com.epam.dir.Directory;
import com.epam.serealization.SerealizationManager;
import com.epam.sourceread.CommentRead;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Logger logger = LogManager.getLogger(View.class);
  private static Scanner input = new Scanner(System.in);

  public View() {
    menu = new LinkedHashMap<>();
    menu.put("1", "Press  1 - execute showBuffer");
    menu.put("2", "Press  2 - execute printPerformance");
    menu.put("3", "Press  3 - execute printDirectory");
    menu.put("4", "Press  4 - execute printSerealization");
    menu.put("5", "Press  5 - execute printCommandRead");
    menu.put("0", "Press  0 - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::showBuffer);
    methodsMenu.put("2", this::printPerformance);
    methodsMenu.put("3", this::printDirectory);
    methodsMenu.put("4", this::printSerealization);
    methodsMenu.put("5", this::printCommandRead);
  }

  private void showBuffer() {
    BufferManager bufferManager = new BufferManager();
    bufferManager.showBuffer();
  }

  private void printPerformance() {
    Performance performance = new Performance();
    performance.comparePerformance();
  }

  private void printDirectory() {
    Directory directory = new Directory();
    try {
      directory.showDir(4, new File("D:\\QA_projects\\task10_IO_NIO"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void printSerealization() {
    SerealizationManager serealizationManager = new SerealizationManager();
    serealizationManager.serealization();
    serealizationManager.deserealization();
  }

  private void printCommandRead() {
    CommentRead commentRead = new CommentRead();
    commentRead.read();
  }

  private void printMenuAction() {
    System.out.println("--------------MENU-----------\n");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void showMenu() {
    String keyMenu;
    do {
      printMenuAction();
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("0"));
  }
}
