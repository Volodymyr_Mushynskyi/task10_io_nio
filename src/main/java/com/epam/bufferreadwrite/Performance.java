package com.epam.bufferreadwrite;

import java.io.*;

public class Performance {

  private long start;
  private long time;

  public void comparePerformance() {
    read();
    write();
  }

  private void read() {
    String line;
    try {
      File file = new File("D:\\QA_projects\\task10_IO_NIO\\src\\main\\resources\\data.txt");
      FileReader fileReader = new FileReader(file);
      BufferedReader bufferedReader = new BufferedReader(fileReader);
      start = System.nanoTime();
      while ((line = bufferedReader.readLine()) != null) {
        System.out.println(line);
      }
      time = System.nanoTime() - start;
      System.out.println("Time " + time / 1000000000.0);
      start = System.nanoTime();
      while (fileReader.ready()) {
        System.out.println(fileReader.read());
      }
      time = System.nanoTime() - start;
      System.out.println("Time " + time / 1000000000.0);
      fileReader.close();
      bufferedReader.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void write() {
    try {
      File file = new File("file.txt");
      FileWriter fileWriter = new FileWriter(file);
      BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
      String[] list = {"one", "two", "three", "fo"};
      start = System.nanoTime();
      for (String s : list) {
        bufferedWriter.write(s + "\n");
      }
      time = System.nanoTime() - start;
      System.out.println("Time " + time);
      start = System.nanoTime();
      for (String s : list) {
        fileWriter.write(s + "\n");
      }
      time = System.nanoTime() - start;
      System.out.println("Time " + time);
      fileWriter.close();
      bufferedWriter.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
