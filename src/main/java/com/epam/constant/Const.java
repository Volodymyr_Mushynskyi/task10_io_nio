package com.epam.constant;

public class Const {
  public static final int BUFFER_SIZE = 1024;
  public static final int PORT = 1000;

  private Const() {

  }
}
