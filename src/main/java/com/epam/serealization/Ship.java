package com.epam.serealization;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Ship implements Serializable {

  private String name;
  private List<Droids> droids = new ArrayList<Droids>();

  public Ship() {

  }

  public Ship(String name) {
    this.name = name;
    createDroids();
  }

  public String getName() {
    return name;
  }

  public List<Droids> getDroids() {
    return droids;
  }

  public void createDroids() {
    droids.add(new Droids("M1", 200));
    droids.add(new Droids("M2", 200));
    droids.add(new Droids("M3", 100));
    droids.add(new Droids("M4", 500));
    droids.add(new Droids("M5", 1000));
  }


}
