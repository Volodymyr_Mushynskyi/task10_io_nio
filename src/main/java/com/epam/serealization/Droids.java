package com.epam.serealization;

import java.io.Serializable;

public class Droids implements Serializable {

  private String name;
  private Integer speed;

  public Droids() {

  }

  public Droids(String name, Integer speed) {
    this.name = name;
    this.speed = speed;
  }

  public String getName() {
    return name;
  }

  public Integer getSpeed() {
    return speed;
  }

}
