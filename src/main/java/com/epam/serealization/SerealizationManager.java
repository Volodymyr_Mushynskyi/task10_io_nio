package com.epam.serealization;

import java.io.*;

public class SerealizationManager {

  public void serealization() {
    Ship ship = new Ship("A");
    try {
      FileOutputStream fileOutputStream = new FileOutputStream("D:\\QA_projects\\task10_IO_NIO\\src\\main\\resources\\ship.data");
      ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);
      out.writeObject(ship);
      out.close();
      fileOutputStream.close();
      System.out.println("Serialization");
      System.out.println("Name" + ship.getName());
      System.out.println("List" + ship.getDroids());
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  public void deserealization() {
    Ship ship = new Ship();
    try {
      FileInputStream fileInputStream = new FileInputStream("D:\\QA_projects\\task10_IO_NIO\\src\\main\\resources\\ship.data");
      ObjectInputStream in = new ObjectInputStream(fileInputStream);
      ship = (Ship) in.readObject();
      in.close();
      fileInputStream.close();
    } catch (IOException io) {
      io.printStackTrace();
      return;
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    System.out.println("Deserialization");
    System.out.println("Name " + ship.getName());
    System.out.println("List " + ship.getDroids());
  }
}

