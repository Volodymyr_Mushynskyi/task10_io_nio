package com.epam.sourceread;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class CommentRead {
  public void read() {
    try {
      File file = new File("D:\\QA_projects\\task10_IO_NIO\\src\\main\\java\\com\\epam\\serealization\\Ship.java");
      FileReader fileReader = new FileReader(file);
      BufferedReader bufferedReader = new BufferedReader(fileReader);
      String line;
      long start1 = System.nanoTime();
      Boolean trigger = false;
      while ((line = bufferedReader.readLine()) != null) {
        if (line.startsWith("//"))
          System.out.println(line);
        if (line.startsWith("/*")) {
          trigger = true;
        }
        if (trigger.equals(true)) {
          System.out.println(line);
        }
        if (line.endsWith("*/")) {
          trigger = false;
        }
      }
      long time1 = System.nanoTime() - start1;
      System.out.println("Time " + time1);
      fileReader.close();
      bufferedReader.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
